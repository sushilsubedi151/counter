import React from 'react';

class ClassCounter extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      count: 1,
    };
  }

  handleClick = () => {
    const updatedCounter = this.state.count + 1;
    this.setState({ count: updatedCounter });
  };

  render() {
    return (
      <div>
        <button onClick={this.handleClick}>
          {this.state.count}
          {this.props.name}
        </button>
      </div>
    );
  }
}

export default ClassCounter;
