import React, { useState, useEffect } from 'react';

const CounterItem = ({ name }) => {
  const [count, setCount] = useState(0);

  useEffect(() => {
    console.log('You called me first time!');
  }, []);

  // value ma depend garda
  useEffect(() => {
    console.log('you called me again!');
  }, [count]);

  return (
    <div>
      <button onClick={() => setCount((prev) => prev + 1)}>
        Count: {count}
      </button>
      {name}
    </div>
  );
};

export default CounterItem;
